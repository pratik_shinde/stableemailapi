This is **Automatic-Email-Sending-API** which accepts JSON object and returns int in the form of HTML convensions.

INPUT SAMPLE :  

{  

	"sendTo" :"abc@xyz.com",            #Sender's Email Address
	"nameOfTheCandidate":"Pranjal",     #Name of the candidate
	"testUrl":"www.google.com",         #URL on which the candidate can take his test
	"username":"manawatp",              #Username of the candidate
	"password":"1234"                   #Password of the candidate
}

OUTPUT SAMPLE :  

200