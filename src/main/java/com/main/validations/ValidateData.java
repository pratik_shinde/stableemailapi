package com.main.validations;

import java.util.regex.Pattern;

public class ValidateData {
	public static boolean validateInputs(String sendTo, String nameOfTheCandidate,String testUrl,String username, String password) {
		if(nameOfTheCandidate.isEmpty() || sendTo.isEmpty() || testUrl.isEmpty() || username.isEmpty() || password.isEmpty())
		{
			return false;
		}
		if(!isValid(sendTo))
		{
			return false;
		}
		return true;
	}
	
	public static boolean isValid(String email) 
    { 
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
                            "[a-zA-Z0-9_+&*-]+)*@" + 
                            "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                            "A-Z]{2,7}$"; 
                              
        Pattern pat = Pattern.compile(emailRegex); 
        if (email == null) 
            return false; 
        return pat.matcher(email).matches(); 
    } 
}
