package com.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.main.controller.MainController;

@SpringBootApplication
public class EmailSendingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmailSendingApiApplication.class, args);
	}

}
