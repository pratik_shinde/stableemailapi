package com.main.controller;

import java.io.IOException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.main.model.Data;
import com.main.service.*;

@RestController
public class MainController {
	@CrossOrigin
	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST,consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	public int sendEmail(@RequestBody Data data) throws IOException
	{
		sendEmail send = new sendEmail();
		return send.composeMessage(data.getSendTo(), data.getNameOfTheCandidate(), data.getTestUrl(), data.getUsername(), data.getPassword());
	}
}
