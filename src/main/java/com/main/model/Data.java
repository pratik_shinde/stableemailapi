package com.main.model;

public class Data {
	public String sendTo, nameOfTheCandidate, testUrl, username, password;

	public String getSendTo() {
		return sendTo;
	}

	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}

	public String getNameOfTheCandidate() {
		return nameOfTheCandidate;
	}

	public void setNameOfTheCandidate(String nameOfTheCandidate) {
		this.nameOfTheCandidate = nameOfTheCandidate;
	}

	public String getTestUrl() {
		return testUrl;
	}

	public void setTestUrl(String testUrl) {
		this.testUrl = testUrl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
