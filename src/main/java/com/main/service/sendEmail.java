package com.main.service;

import java.util.*;
import com.main.validations.*;
import javax.mail.*;
import javax.mail.internet.*;

import com.main.validations.ValidateData;

public class sendEmail {
	final String senderUsername = "test.genesisportal@gmail.com";
    final String senderPassword = "Cdktest1234";

    protected Session createSession(){
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(senderUsername, senderPassword);
                    }
                });

        return session;
    }

    public int composeMessage(String sendTo, String nameOfTheCandidate,String testUrl,String username, String password) {
        if(ValidateData.validateInputs(sendTo,nameOfTheCandidate,testUrl,username,password)) {
        	 MimeMessage message = new MimeMessage(createSession());
             try {
				message.setFrom(new InternetAddress(senderUsername));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(sendTo));
	             message.setSubject("CDK Global online coding test");
	             message.setText("Hey "+nameOfTheCandidate+",Please click on the link below to take a test now..\n"+testUrl+"\nUsername : "+username+"\npassword : "+password+"\nAll the best.. See you soon on board..!");

	             //send the message
	             Transport.send(message);
	             return 200;
			} catch (AddressException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return 404;
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return 404;
			}
             
        }
        else {
        	return 500;
        }
    }
}
